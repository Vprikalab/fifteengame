<?php

use Illuminate\Support\Facades\Route;

Route::post('create', 'GameController@create')->name('create');


Route::middleware(['can:update,game'])->group(function () {

    Route::post('{game}/solve', 'GameController@solve')->name('solve');

    Route::get('{game}/solve', 'GameController@getSolve')->name('solve');

});
