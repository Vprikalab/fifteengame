<?php

namespace App\Models;

use App\Exceptions\BoardState\OutOfRangeException;
use App\Exceptions\BoardState\TryingMoveNotEmptyCell;
use Illuminate\Database\Eloquent\Model;

class BoardState extends Model
{
    protected $fillable = [
        'state',
        'game_id'
    ];

    protected $casts = [
        'state' => 'array'
    ];

    protected $zeroX;

    protected $zeroY;

    public function game()
    {
        return $this->belongsTo(Game::class);
    }

    protected function findZeroCoordinates()
    {
        if (!isset($this->zeroY) || !isset($this->zeroX)) {
            foreach ($this->state as $rowKey => $row) {
                foreach ($row as $columnKey => $column) {
                    if ($column == null) {
                        $this->zeroX = $rowKey;
                        $this->zeroY = $columnKey;
                    }
                }
            }
        }
    }

    public function zeroX(): int
    {
        $this->findZeroCoordinates();

        return $this->zeroX;
    }

    public function zeroY(): int
    {
        $this->findZeroCoordinates();

        return $this->zeroY;
    }

    public function columnLength()
    {
        return count($this->state[0]) - 1;
    }

    /**
     * @throws OutOfRangeException
     */
    public function swipeDown()
    {
        if ($this->zeroX() == $this->columnLength()) {
            throw new OutOfRangeException();
        }

        $this->replace($this->zeroX(), $this->zeroY(), $this->zeroX() + 1, $this->zeroY());
    }

    public function swipeUp()
    {
        if ($this->zeroX() == 0) {
            throw new OutOfRangeException();
        }

        $this->replace($this->zeroX(), $this->zeroY(), $this->zeroX() - 1, $this->zeroY());
    }

    public function swipeRight()
    {
        if ($this->zeroY() == $this->columnLength()) {
            throw new OutOfRangeException();
        }

        $this->replace($this->zeroX(), $this->zeroY(), $this->zeroX(), $this->zeroY() + 1);
    }

    public function swipeLeft()
    {
        if ($this->zeroY() == 0) {
            throw new OutOfRangeException();
        }

        $this->replace($this->zeroX(), $this->zeroY(), $this->zeroX(), $this->zeroY() - 1);

    }

    /**
     * Replace element of board
     * @param int $fromX
     * @param int $fromY
     * @param int $toX
     * @param int $toY
     */
    public function replace(int $fromX, int $fromY, int $toX, int $toY) {

        if (!is_null($this->state[$fromX][$fromY])) {
            throw new TryingMoveNotEmptyCell();
        }

        $oldValue = $this->state[$fromX][$fromY];

        $stateCopy = $this->state;

        $stateCopy[$fromX][$fromY] = $stateCopy[$toX][$toY];

        $stateCopy[$toX][$toY] = $oldValue;

        $this->zeroY = $toY;

        $this->zeroX = $toX;

        $this->state = $stateCopy;
    }
}
