<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Game extends Model
{
    protected $fillable = [
        'dictionary',
        'user_id',
        'column_length'
    ];

    protected $casts = [
        'dictionary' => 'array'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function boardStates()
    {
        return $this->hasMany(BoardState::class)->orderBy('created_at', 'DESC');
    }
}
