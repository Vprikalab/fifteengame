<?php


namespace App\Services\Abstracts;


use App\Models\Game;
use App\Models\User;
use Illuminate\Support\Collection;

interface GameInterface
{
    /**
     * Create new Game
     * @param array $dictionary
     * @param User $user
     * @return Game
     */
    public function create(array $dictionary, User $user): Game;

    /**
     * @param array $moves
     * @param Game $game
     * @return bool
     */
    public function solve(array $moves, Game $game): Game;

    /**
     * @param Game $game
     * @return Collection
     */
    public function getSolve(Game $game): Collection;

}
