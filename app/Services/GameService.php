<?php


namespace App\Services;


use App\Domain\GameSolver\Board;
use App\Domain\GameSolver\Stack;
use App\Exceptions\BoardState\OutOfRangeException;
use App\Models\BoardState;
use App\Models\Game;
use App\Models\User;
use App\Services\Abstracts\GameInterface;
use Exception;
use Illuminate\Support\Collection;

class GameService implements Abstracts\GameInterface
{
    /** @inheritDoc */
    public function create(array $dictionary, User $user): Game
    {
        $columnLength = sqrt(count($dictionary) + 1);

        $dictionary = array_chunk($dictionary, $columnLength);

        $dictionary[$columnLength - 1][$columnLength - 1] = null;

        $game = new Game([
            'dictionary' => $dictionary,
            'user_id' => $user->getKey(),
            'column_length' => $columnLength
        ]);

        $game->save();

        $boardState = new BoardState([
            'state' => $game->dictionary,
            'game_id' => $game->getKey(),
        ]);

        $this->shuffle($boardState);

        $boardState->save();

        return $game;
    }

    /** @inheritDoc */
    public function solve(array $moves, Game $game): Game
    {
        foreach ($moves as $move) {
            $boardState = $game->boardStates()->create([
                'state' => $game->boardStates->first()->state
            ]);

            $boardState->$move();

            $boardState->save();
        }

        if ($game->dictionary == $game->boardStates->first()->state) {
            $game->solved_at = now();
            $game->save();
        }

        $game = Game::find($game->getKey());

        return $game;
    }

    /**
     * @param Game $game
     * @return Collection
     */
    public function getSolve(Game $game): Collection
    {
        $lastBoardState = $game->boardStates->first();

        $board = new Board(null, $lastBoardState->state, $game->dictionary);

        $stack = new Stack();

        $stack->add($board->h(), $board);

        $rowLen = $lastBoardState->columnLength();

        $iteration = 0;

        while (true) {
            $board = $stack->getFirst();

            $iteration++;

            if ($board->h() == 0) {
                break;
            }

            $zeroX = $board->getZeroCoordinates()['x'];

            $zeroY = $board->getZeroCoordinates()['y'];

            if ($zeroX != 0) {

                try {
                    $newBoard = $board->getChangedBoard($zeroX, $zeroY, $zeroX - 1, $zeroY);

                    $stack->add($newBoard->h(), $newBoard);
                } catch (Exception $exception) {

                }
            }

            if ($zeroY != 0) {

                try {
                    $newBoard = $board->getChangedBoard($zeroX, $zeroY, $zeroX, $zeroY - 1);

                    $stack->add($newBoard->h(), $newBoard);
                } catch (Exception $exception) {

                }

            }

            if ($zeroX < ($rowLen - 1)) {

                try {

                    $newBoard = $board->getChangedBoard($zeroX, $zeroY, $zeroX + 1, $zeroY);

                    $stack->add($newBoard->h(), $newBoard);
                } catch (Exception $exception) {

                }
            }

            if ($zeroY < ($rowLen - 1)) {
                try {

                    $newBoard = $board->getChangedBoard($zeroX, $zeroY, $zeroX, $zeroY + 1);

                    $stack->add($newBoard->h(), $newBoard);
                } catch (Exception $exception) {

                }
            }
        }

        $solve = collect([$board]);

        while ($board->getPrevious()) {
            $previous = $board->getPrevious();
            $solve->add($previous);
            $board = $previous;
        }

        return $solve;
    }

    /**
     * Shuffle game field
     * @param BoardState $boardState
     * @return BoardState
     */
    private function shuffle(BoardState &$boardState): BoardState {

        for($i = 0; $i < 50; $i++) {
            try {
                switch (rand(0, 3)) {
                    case 0:
                        $boardState->swipeUp();
                        break;
                    case 1:
                        $boardState->swipeDown();
                        break;
                    case 2:
                        $boardState->swipeLeft();
                        break;
                    case 3:
                        $boardState->swipeRight();
                        break;
                }
            } catch (OutOfRangeException $exception) {}
        }

        return $boardState;
    }
}
