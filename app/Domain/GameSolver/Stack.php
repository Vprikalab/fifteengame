<?php

namespace App\Domain\GameSolver;

class Stack implements \App\Domain\GameSolver\Abstracts\Stack
{
    protected $collection;

    public function __construct()
    {
        $collection = [];
    }

    /**
     * @inheritDoc
     */
    public function add(int $cost, \App\Domain\GameSolver\Abstracts\Board $board)
    {
        $this->collection[$cost][] = $board;
    }

    /**
     * @inheritDoc
     */
    public function getFirst(): \App\Domain\GameSolver\Abstracts\Board
    {
        if (empty($this->collection[array_key_first($this->collection)])) {
            unset($this->collection[array_key_first($this->collection)]);
            return $this->getFirst();
        }
        return array_shift($this->collection[array_key_first($this->collection)]);
    }
}
