<?php

namespace App\Domain\GameSolver\Abstracts;

interface Board
{
    public function h(): int;

    public function g(): int;

    public function getPrevious();

    public function getZeroCoordinates();

    public function getChangedBoard(int $fromX, int $fromY, int $toX, int $toY): \App\Domain\GameSolver\Board;
}
