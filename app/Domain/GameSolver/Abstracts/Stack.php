<?php


namespace App\Domain\GameSolver\Abstracts;


interface Stack
{
    /**
     * @param int $cost
     * @param Board $board
     * @return mixed
     */
    public function add(int $cost, Board $board);

    /**
     * @return Board
     */
    public function getFirst(): Board;
}
