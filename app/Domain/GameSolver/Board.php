<?php

namespace App\Domain\GameSolver;

class Board implements \App\Domain\GameSolver\Abstracts\Board
{
    /** @var Board  */
    protected $previous;

    public $positions;

    protected $solve;

    protected $g;

    protected $h;

    protected $zeroX;

    protected $zeroY;

    /**
     * Board constructor.
     * @param Board $previous
     * @param array $positions
     * @param array $solve
     */
    public function __construct(Board $previous = null, array $positions, array $solve)
    {
        $this->previous = $previous;

        $this->g = $previous ? $previous->g + 1 : 0;

        $this->positions = $positions;

        $this->solve = $solve;

        $this->h = $this->calculateH();

        $zeroCoordinates = $this->calculateZeroCoordinates();

        $this->zeroX = $zeroCoordinates['x'];

        $this->zeroY = $zeroCoordinates['y'];
    }

    public function g(): int
    {
        return $this->g;
    }

    public function h(): int
    {
        return $this->h;
    }

    public function getPrevious()
    {
        return $this->previous;
    }

    public function getZeroCoordinates()
    {
        return [
            'x' => $this->zeroX,
            'y' => $this->zeroY
        ];
    }

    public function getChangedBoard(int $fromX, int $fromY, int $toX, int $toY): Board
    {

        if (!empty($this->previous)) {
            $oldX = $this->previous->getZeroCoordinates()['x'];
            $oldY = $this->previous->getZeroCoordinates()['y'];

            if ((($oldX == $toX && $oldY == $toY) || ($oldX == $fromX && $oldY == $fromY))) {
                throw new \Exception();
            }
        }

        $newPositions = $this->positions;

        $number = $newPositions[$toX][$toY];

        $newPositions[$toX][$toY] = $newPositions[$fromX][$fromY];

        $newPositions[$fromX][$fromY] = $number;

        return new Board($this, $newPositions, $this->solve);
    }

    /**
     * @return int
     */
    protected function calculateH(): int
    {
        $result = 0;

        foreach($this->positions as $stringKey=>$stringValue) {
            foreach($stringValue as $rowKey=>$rowValue) {
                if ($this->solve[$stringKey][$rowKey] != $rowValue) {
                    $result++;
                }
            }
        }

        return $result;
    }

    protected function calculateZeroCoordinates(): array
    {
        foreach($this->positions as $stringKey=>$stringValue) {
            foreach($stringValue as $rowKey=>$rowValue) {
                if ($rowValue == null) {
                    return [
                        'x' => $stringKey,
                        'y' => $rowKey
                    ];
                }
            }
        }
    }
}
