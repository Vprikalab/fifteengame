<?php

namespace App\Policies;

use App\Models\Game;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;
use Illuminate\Auth\Access\Response;

class GamePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * @param User $user
     * @param Game $game
     * @return Response
     */
    public function update(User $user, Game $game)
    {
        return $user->getKey() == $game->user_id ?
            Response::allow() :
            Response::deny();
    }
}
