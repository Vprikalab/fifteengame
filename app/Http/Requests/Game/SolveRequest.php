<?php

namespace App\Http\Requests\Game;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class SolveRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'actions' => ['array', 'required'],
            'actions.*' => [
                'required',
                'string',
                Rule::in(['swipeUp', 'swipeDown', 'swipeLeft', 'swipeRight'])
            ]
        ];
    }
}
