<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\Game\CreateRequest;
use App\Http\Requests\Game\SolveRequest;
use App\Http\Resources\Game\SolveResource;
use App\Http\Resources\Game\WithStateResultResource;
use App\Models\Game;
use App\Services\Abstracts\GameInterface;
use Illuminate\Http\Request;

class GameController extends Controller
{
    /**
     * @var GameInterface
     */
    protected $gameService;

    /**
     * GameController constructor.
     * @param GameInterface $gameService
     */
    public function __construct(GameInterface $gameService)
    {
        $this->gameService = $gameService;
    }

    /**
     * @param CreateRequest $request
     * @return WithStateResultResource
     */
    public function create(CreateRequest $request)
    {
        if ($request->has('field')) {
            $field = $request->field;
        } else {
            $field = 'qwertyui';
        }

        $game = $this->gameService->create(str_split($field), $request->user());

        return new WithStateResultResource($game);
    }

    /**
     * @param SolveRequest $request
     * @param Game $game
     * @return WithStateResultResource
     */
    public function solve(SolveRequest $request, Game $game)
    {
        $game = $this->gameService->solve($request->actions, $game);

        return new WithStateResultResource($game);
    }

    /**
     * @param Game $game
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function getSolve(Game $game)
    {
        $solve = $this->gameService->getSolve($game);

        return SolveResource::collection($solve);
    }
}
