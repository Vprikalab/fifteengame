<?php


namespace App\Exceptions\BoardState;


class TryingMoveNotEmptyCell extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }
}
