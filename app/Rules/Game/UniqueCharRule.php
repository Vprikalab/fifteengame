<?php

namespace App\Rules\Game;

use Illuminate\Contracts\Validation\Rule;

class UniqueCharRule implements Rule
{
    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $existChars = [];

        foreach (str_split($value) as $char) {
            if (in_array($char, $existChars)) {
                return false;
            }

            $existChars[] = $char;
        }

        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return __('game.errors.equalChars');
    }
}
