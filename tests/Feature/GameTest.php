<?php

namespace Tests\Feature;

use App\Models\User;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class GameTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Create new Game without field
     */
    public function testGameCreateWithoutField()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->postJson(route('game.create'), []);

        $response->assertJsonStructure(['data' => ['game_id', 'board_state']]);
    }

    /**
     * Create new Game with wrong field
     */
    public function testGameCreateWithWrongField()
    {
        $user = factory(User::class)->create();

        $response = $this->actingAs($user)->postJson(route('game.create'), [
            'field' => 'asdasd'
        ]);

        $response->assertStatus(422);
    }
}
